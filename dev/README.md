# Scripts

## Description
All data processing was done in [**R**](https://cran.r-project.org/). The
following scripts were used for the analysis and for creating output files for
further plotting:

- `completeness_analysis.R`
- `trends_in_concentration.R`
- `trends_in_frequency.R`
- `trends_in_seasonality.R`

Script file names correspond to the respective sections in the manuscript.

While exploratory plots were done directly in R using base graphics and ggplot2,
respectively, plots in the manuscript were done using the LaTeX package
[PGFPlots](http://pgfplots.sourceforge.net/). A short shell script was used to
convert the LaTeX output from PDF to PNG with 300 dpi.

- `pdf_to_png.sh`


## R Session Info
```R
> sessioninfo::session_info()
```
```
─ Session info ─────────────────────────────────────────────────────────────────
 setting  value                       
 version  R version 4.1.0 (2021-05-18)
 os       Manjaro Linux               
 system   x86_64, linux-gnu           
 ui       RStudio                     
 language (EN)                        
 collate  en_US.UTF-8                 
 ctype    en_US.UTF-8                 
 tz       Europe/Vienna               
 date     2021-07-09                  

─ Packages ─────────────────────────────────────────────────────────────────────
 package        * version date       lib source        
 assertthat       0.2.1   2019-03-21 [1] CRAN (R 4.1.0)
 backports        1.2.1   2020-12-09 [1] CRAN (R 4.1.0)
 bcp            * 4.0.3   2018-08-13 [1] CRAN (R 4.1.0)
 boot           * 1.3-28  2021-05-03 [2] CRAN (R 4.1.0)
 breakpoint     * 1.2     2016-01-13 [1] CRAN (R 4.1.0)
 broom            0.7.8   2021-06-24 [1] CRAN (R 4.1.0)
 cellranger       1.1.0   2016-07-27 [1] CRAN (R 4.1.0)
 changepoint    * 2.2.2   2016-10-04 [1] CRAN (R 4.1.0)
 changepoint.np * 1.0.3   2021-01-20 [1] CRAN (R 4.1.0)
 circular       * 0.4-93  2017-06-29 [1] CRAN (R 4.1.0)
 cli              3.0.0   2021-06-30 [1] CRAN (R 4.1.0)
 codetools        0.2-18  2020-11-04 [2] CRAN (R 4.1.0)
 colorspace       2.0-2   2021-06-24 [1] CRAN (R 4.1.0)
 conquer          1.0.2   2020-08-27 [1] CRAN (R 4.1.0)
 crayon           1.4.1   2021-02-08 [1] CRAN (R 4.1.0)
 DBI              1.1.1   2021-01-15 [1] CRAN (R 4.1.0)
 dbplyr           2.1.1   2021-04-06 [1] CRAN (R 4.1.0)
 digest           0.6.27  2020-10-24 [1] CRAN (R 4.1.0)
 doParallel       1.0.16  2020-10-16 [1] CRAN (R 4.1.0)
 dplyr          * 1.0.7   2021-06-18 [1] CRAN (R 4.1.0)
 ellipsis         0.3.2   2021-04-29 [1] CRAN (R 4.1.0)
 expm             0.999-6 2021-01-13 [1] CRAN (R 4.1.0)
 fANCOVA        * 0.6-1   2020-11-13 [1] CRAN (R 4.1.0)
 fansi            0.5.0   2021-05-25 [1] CRAN (R 4.1.0)
 farver           2.1.0   2021-02-28 [1] CRAN (R 4.1.0)
 forcats        * 0.5.1   2021-01-27 [1] CRAN (R 4.1.0)
 foreach          1.5.1   2020-10-15 [1] CRAN (R 4.1.0)
 fs               1.5.0   2020-07-31 [1] CRAN (R 4.1.0)
 generics         0.1.0   2020-10-31 [1] CRAN (R 4.1.0)
 ggplot2        * 3.3.5   2021-06-25 [1] CRAN (R 4.1.0)
 glue             1.4.2   2020-08-27 [1] CRAN (R 4.1.0)
 gtable           0.3.0   2019-03-25 [1] CRAN (R 4.1.0)
 haven            2.4.1   2021-04-23 [1] CRAN (R 4.1.0)
 hms              1.1.0   2021-05-17 [1] CRAN (R 4.1.0)
 httr             1.4.2   2020-07-20 [1] CRAN (R 4.1.0)
 iterators        1.0.13  2020-10-15 [1] CRAN (R 4.1.0)
 jsonlite         1.7.2   2020-12-09 [1] CRAN (R 4.1.0)
 Kendall        * 2.2     2011-05-18 [1] CRAN (R 4.1.0)
 labeling         0.4.2   2020-10-20 [1] CRAN (R 4.1.0)
 lattice          0.20-44 2021-05-02 [2] CRAN (R 4.1.0)
 lifecycle        1.0.0   2021-02-15 [1] CRAN (R 4.1.0)
 lubridate        1.7.10  2021-02-26 [1] CRAN (R 4.1.0)
 magrittr         2.0.1   2020-11-17 [1] CRAN (R 4.1.0)
 MASS             7.3-54  2021-05-03 [2] CRAN (R 4.1.0)
 Matrix           1.3-3   2021-05-04 [2] CRAN (R 4.1.0)
 MatrixModels     0.5-0   2021-03-02 [1] CRAN (R 4.1.0)
 matrixStats      0.59.0  2021-06-01 [1] CRAN (R 4.1.0)
 mblm           * 0.12.1  2019-01-26 [1] CRAN (R 4.1.0)
 modelr           0.1.8   2020-05-19 [1] CRAN (R 4.1.0)
 msm              1.6.8   2019-12-16 [1] CRAN (R 4.1.0)
 munsell          0.5.0   2018-06-12 [1] CRAN (R 4.1.0)
 mvtnorm          1.1-2   2021-06-07 [1] CRAN (R 4.1.0)
 pillar           1.6.1   2021-05-16 [1] CRAN (R 4.1.0)
 pkgconfig        2.0.3   2019-09-22 [1] CRAN (R 4.1.0)
 purrr          * 0.3.4   2020-04-17 [1] CRAN (R 4.1.0)
 quantreg       * 5.86    2021-06-06 [1] CRAN (R 4.1.0)
 R6               2.5.0   2020-10-28 [1] CRAN (R 4.1.0)
 randtests      * 1.0     2014-11-17 [1] CRAN (R 4.1.0)
 Rcpp             1.0.6   2021-01-15 [1] CRAN (R 4.1.0)
 readr          * 1.4.0   2020-10-05 [1] CRAN (R 4.1.0)
 readxl           1.3.1   2019-03-13 [1] CRAN (R 4.1.0)
 reprex           2.0.0   2021-04-02 [1] CRAN (R 4.1.0)
 rlang            0.4.11  2021-04-30 [1] CRAN (R 4.1.0)
 rstudioapi       0.13    2020-11-12 [1] CRAN (R 4.1.0)
 rvest            1.0.0   2021-03-09 [1] CRAN (R 4.1.0)
 scales           1.1.1   2020-05-11 [1] CRAN (R 4.1.0)
 sessioninfo      1.1.1   2018-11-05 [1] CRAN (R 4.1.0)
 SparseM        * 1.81    2021-02-18 [1] CRAN (R 4.1.0)
 stringi          1.6.2   2021-05-17 [1] CRAN (R 4.1.0)
 stringr        * 1.4.0   2019-02-10 [1] CRAN (R 4.1.0)
 survival         3.2-11  2021-04-26 [2] CRAN (R 4.1.0)
 tibble         * 3.1.2   2021-05-16 [1] CRAN (R 4.1.0)
 tidyr          * 1.1.3   2021-03-03 [1] CRAN (R 4.1.0)
 tidyselect       1.1.1   2021-04-30 [1] CRAN (R 4.1.0)
 tidyverse      * 1.3.1   2021-04-15 [1] CRAN (R 4.1.0)
 utf8             1.2.1   2021-03-12 [1] CRAN (R 4.1.0)
 vctrs            0.3.8   2021-04-29 [1] CRAN (R 4.1.0)
 withr            2.4.2   2021-04-18 [1] CRAN (R 4.1.0)
 xml2             1.3.2   2020-04-23 [1] CRAN (R 4.1.0)
 zoo            * 1.8-9   2021-03-09 [1] CRAN (R 4.1.0)

[1] /home/ms/R/x86_64-pc-linux-gnu-library/4.1
[2] /usr/lib/R/library
```
