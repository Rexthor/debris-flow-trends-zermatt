# Fallacies in Debris Flow Trend Analysis

This repository supplements the comment by
Micha Heiser <sup>[![](https://info.orcid.org/wp-content/uploads/2020/12/orcid_16x16.gif)](https://orcid.org/0000-0002-8675-0579)</sup>,
Matthias Schlögl <sup>[![](https://info.orcid.org/wp-content/uploads/2020/12/orcid_16x16.gif)](https://orcid.org/0000-0002-4357-523X)</sup>,
Christian Scheidl <sup>[![](https://info.orcid.org/wp-content/uploads/2020/12/orcid_16x16.gif)](https://orcid.org/0000-0002-5625-6238)</sup> and
Sven Fuchs <sup>[![](https://info.orcid.org/wp-content/uploads/2020/12/orcid_16x16.gif)](https://orcid.org/0000-0002-0644-2876)</sup> (2022): **Fallacies in Debris Flow Trend Analysis**. *Journal of Geophysical Research: Earth Surface*, 127(3), e2021JF006562. [doi:10.1029/2021JF006562](https://doi.org/10.1029/2021JF006562)

## Highlights
> - We comment on the implications between exploratory and inferential data analysis of debris flow event time series by means of an example.
> - We illustrate why the completeness of event time series is a crucial requirement for analyzing trends.
> - We outline approaches that foster drawing robust conclusions on temporal trends.

## Companion papers
The comment is related to the following publications:
- Schneuwly-Bollschweiler, M., and Stoffel, M. (2012):
  **Hydrometeorological triggers of periglacial debris flows in the Zermatt valley (Switzerland) since 1864**.
  *J. Geophys. Res.*, 117, F02033. [doi:10.1029/2011JF002262](https://doi.org/10.1029/2011JF002262).
- Heiser, M., Hübl, J., and Scheidl, C. (2019):
  **Completeness analyses of the Austrian torrential event catalog**.
  *Landslides*, 16, 2115–2126. [doi:10.1007/s10346-019-01218-3](https://doi.org/10.1007/s10346-019-01218-3).
- Schlögl, M., Fuchs, S., Scheidl, C., and Heiser, M. (2021):
  **Trends in torrential flooding in the Austrian Alps: A combination of climate change, exposure dynamics, and mitigation measures**.
  *Clim. Risk Manag.*, 32, 100294. [doi:10.1016/j.crm.2021.100294](https://doi.org/10.1016/j.crm.2021.100294).


## Data
Data on *Date, dating quality, and rainfall data for all debris flows* of the
Zermatt valley is available as *Table S1* in the Supporting Information of
[doi:10.1029/2011JF002262](https://doi.org/10.1029/2011JF002262).
See [dat/README.md](dat/README.md) for details.

 
## Setup

- Structure of the repo:
    - `dat`: data sets
    - `dev`: development/code
    - `doc`: documentation
    - `plt`: plots

- The repo structure is loosely based on the
  [Cookiecutter Data Science](https://drivendata.github.io/cookiecutter-data-science/)

- Exploratory analysis of the data was done in **R**.
  The following R packages are required to run all the code in this repo:

  ```R
  # General data handling
  install.packages("tidyverse")
  # Completeness analysis
  install.packages("bcp")
  install.packages("breakpoint")
  install.packages("changepoint.np")
  # Frequency
  install.packages("fANCOVA")
  # Seasonality
  install.packages("boot")
  install.packages("fANCOVA")
  install.packages("Kendall")
  install.packages("mblm")
  install.packages("quantreg")
  install.packages("randtests")
  # Concentration
  install.packages("circular")
  ```

- The coding style adheres to the [tidyverse style guide](https://style.tidyverse.org/)
  using [`styler`](https://styler.r-lib.org/).
