# Data set

## Metadata
- **Description**: Debris flow events for the Zermatt Valley, Switzerland.
  The dataset is based on different sources and includes information
  on date of occurrence, corresponding data quality (uncertainty) and rainfall sums.
- **Source**:
    - Schneuwly-Bollschweiler, M., and Stoffel, M. (2012): Hydrometeorological triggers of periglacial debris flows in the Zermatt valley (Switzerland) since 1864.
      *J. Geophys. Res.*, 117, F02033. [doi:10.1029/2011JF002262](https://doi.org/10.1029/2011JF002262)
    - Supporting Information, Table S1: Date, dating quality, and rainfall data for all debris flows.
      Direct link: [jgrf948-sup-0002-ts01.pdf](https://agupubs.onlinelibrary.wiley.com/action/downloadSupplement?doi=10.1029%2F2011JF002262&file=jgrf948-sup-0002-ts01.pdf)

## Dataset structure

| Column    | description                                     |
| :-------- | ----------------------------------------------- |
| `Year`    | year of debris flow occurence                   |
| `Month`   | month of debris flow occurence                  |
| `Day`     | day of debris flow occurence                    |
| `Quality` | uncertainty / possible error of date in days\*  |
| `T1`      | 1 if torrent had a debris flow, empty otherwise |
| `T2`      | 2 if torrent had a debris flow, empty otherwise |
| `T3`      | 3 if torrent had a debris flow, empty otherwise |
| `T4`      | 4 if torrent had a debris flow, empty otherwise |
| `T5`      | 5 if torrent had a debris flow, empty otherwise |
| `T6`      | 6 if torrent had a debris flow, empty otherwise |
| `T7`      | 7 if torrent had a debris flow, empty otherwise |
| `T8`      | 8 if torrent had a debris flow, empty otherwise |
| `G24h`    | 24 h rainfall sum of the Grächen station        |
| `G48h`    | 48 h rainfall sum of the Grächen station        |
| `G72h`    | 72 h rainfall sum of the Grächen station        |
| `Z24h`    | 24 h rainfall sum of the Zermatt station        |
| `Z48h`    | 48 h rainfall sum of the Zermatt station        |
| `Z72h`    | 72 h rainfall sum of the Zermatt station        |
| `A24h`    | 24 h rainfall sum of the Ackersand station      |
| `A48h`    | 48 h rainfall sum of the Ackersand station      |
| `A72h`    | 72 h rainfall sum of the Ackersand station      |

\* Note: `Quality` is based on
- archives (`Quality` = 0; original dataset = `***`),
- highly probable, i.e. based on rainfall and dendrodata (`Quality` = 7; original dataset = `**`),
- possible (`Quality` = 14; original dataset = `*`).

